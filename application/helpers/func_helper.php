<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function _curl_data($url,$data = array()){ //$data = array('url'=>$link_video_n);
    $ch = @curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2062.124 Safari/537.36');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
    //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:', $bearer));
    if($data!=null){
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}


function _myRedirectJs($url= "", $sec=1){
    $url == "" ? $url = $_SERVER['HTTP_REFERER'] : "";
    $sec = $sec*1000;
    $str = '<script>
            setTimeout("myrefresh()", "'.$sec.'");
            
            function myrefresh(){
                location.href = "'.$url.'";
            }
            
        </script>';
    return $str;
}

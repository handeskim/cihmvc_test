<!doctype html>
<html lang="en">

<head>
    <title>Quản lý người dùng</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <link rel="stylesheet" href="/public/fontawesome/css/all.css">

    <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">
    <script src="/public/jquery-3.2.1.min.js"></script>

    <style type="text/css">
        .require {
            color: red;
        }
        .hlist{
            margin-bottom: 20px;
        }
    </style>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="/public/js/dev.js"></script>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Quản lý người dùng</h1>
                <hr />
            </div>
        </div>

        
        <div class="row">
            <div id="message"></div>
            <div class="col-12">
                <h3>Thêm người dùng mới</h3>
                <form method="post">
                    <div class="form-group">
                        <label for="Ho_ten">Họ tên: <span class="require">*</span></label>
                        <input type="text" class="form-control" id="Ho_ten" name="Ho_ten" placeholder="Nhập Họ tên">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="Ngay_sinh">Ngày sinh: <span class="require">*</span></label>
                        <div class="input-group">
                            <input type="text" class="form-control" id="Ngay_sinh" name="Ngay_sinh" placeholder="Nhập Ngày sinh: dd-mm-yyyy" readonly data-date-format="dd-mm-yyyy">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar bigger-110"></i>
                            </span>
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="Gioi_tinh">Giới tính: <span class="require">*</span></label>
                        <select class="form-control" id="Gioi_tinh" name="Gioi_tinh">
                            <option value="Nam">Nam</option>
                            <option value="Nữ">Nữ</option>

                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="Balancer">Số dư tài khoản: <span class="require">*</span></label>
                        <input type="text" class="form-control" id="Balancer" name="Balancer" placeholder="Nhập Số dư">
                    </div>
                    <!-- <input type="hidden" name="<?//=$csrf['name'];?>" value="<?//=$csrf['hash'];?>" /> -->
                    <div class="form-group text-center">
                        <button id="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>

        <hr />
        <div class="row" style="margin-bottom: 20px;">
            <div class="col-12">
                <h2 class="hlist">Danh sách người dùng</h2>
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>Họ Tên</th>
                            <th>Ngày Sinh</th>
                            <th>Giới Tính</th>
                            <th>Số dư</th>
                            <th>Ngày tạo</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Họ Tên</th>
                            <th>Ngày Sinh</th>
                            <th>Giới Tính</th>
                            <th>Số dư</th>
                            <th>Ngày tạo</th>
                        </tr>
                    </tfoot>
                </table>


            </div>
        </div>

    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
        $(function() {
            $("#Ngay_sinh").datepicker({
                autoclose: true,
                todayHighlight: true
            });
        });
    </script>


</body>

</html>
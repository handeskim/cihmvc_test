<?php
class Base extends MY_Controller{
	function __construct(){
		parent::__construct();
		
		$this->load->library('rest');
		$config =  array('server' => base_url('api'));
		$this->rest->initialize($config);
        $this->data = array('title'=> 'Base Chào mừng đến bài test giả lập',);
        
        //$this->_tbl = 'tbl_users';
        $this->_tbl = 'tbl_users';
    }
    
	public function index(){
        
		$this->data['param'] = array('name'=>'HK Team','year'=>date('Y',time()));
		$this->parser->parse('base',$this->data);
	}
	
}
?>
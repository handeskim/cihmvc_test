<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
class Api extends REST_Controller {
	function __construct(){
		parent::__construct();
		$this->r = array('status'=>200,'message'=>'error');
        
        $this->_tbl = 'tbl_users';
        //$this->_tbl = 'tbl_test';
	}
	public function index_get(){
        $this -> r['type'] = 'get';
        $data = [];
        $rows= $this ->mongo_db->get($this->_tbl);
       
        if(!empty($rows)){
            foreach ($rows as $r){
                $data[] = [
                    "Ho_ten"          => isset($r["Ho_ten"])?$r["Ho_ten"]:'',
                    "Ngay_sinh"      => isset($r["Ngay_sinh"])?$r["Ngay_sinh"]:'',
                    "Gioi_tinh"        => isset($r["Gioi_tinh"])?$r["Gioi_tinh"]:'',
                    "Balancer"       => isset($r["Balancer"])?$r["Balancer"]:'',
                    "Date_create"    => isset($r["Date_create"])?date('H:i d-m-Y',$r["Date_create"]):'',
                ];
            }
        }
        $this->r['message'] = 'success';
        $this->r['data'] = $data;
		$this->response($this->r);
	}
	public function index_post(){
        $this -> r['type'] = 'post';

  
        $hoten = $this -> input -> get_post('hoten');
        $ngaysinh = $this -> input -> get_post('ngaysinh');
        $gioitinh = $this -> input -> get_post('gioitinh');
        $balancer = $this -> input -> get_post('balancer');

        if(empty($hoten)){
            $this->r['message'] = 'Họ tên không được rỗng';
            return $this->response($this->r);
        }   
        if(!is_string($hoten)){
            $this->r['message'] = 'Họ tên phải là chuỗi';
            return $this->response($this->r);
        }
        if(empty($ngaysinh)){
            $this->r['message'] = 'Ngày sinh không được rỗng';
            return $this->response($this->r);
        }  
        if (!preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-[0-9]{4}$/",$ngaysinh)) {
            $this->r['message'] = 'Ngày sinh phải định dạng dd-mm-yyyy';
            return $this->response($this->r);
        }
        if(empty($gioitinh)){
            $this->r['message'] = 'Giới tính không được rỗng';
            return $this->response($this->r);
        }
        if(!is_string($gioitinh)){
            $this->r['message'] = 'Giới tính phải là chuỗi';
            return $this->response($this->r);
        }
        if(empty($balancer)){
            $this->r['message'] = 'Số dư không được rỗng và lớn hơn 0';
            return $this->response($this->r);
        }
        if(!is_float($balancer) && !is_numeric($balancer)){
            $this->r['message'] = 'Số dư phải là số và lớn hơn 0';
            return $this->response($this->r);
        }

        $options = array(
            'Ho_ten' => $hoten,
            'Ngay_sinh' => $ngaysinh,
            'Gioi_tinh' => $gioitinh,
            'Balancer' => floatval($balancer),
            'Date_create' => time(),
        );

        $this -> mongo_db -> insert($this->_tbl, $options);
        $this->r['message'] = 'success';
        
        $this->response($this->r);
	}
	public function index_put(){
		$this->response($this->r);
	}
	public function index_del(){
		$this->response($this->r);
	}
}	

?>